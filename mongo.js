const mongoose = require('mongoose')

const exitWithError = (errorMsg) => {
    console.log(errorMsg)
    process.exit(1)
}

const argsLength = process.argv.length

if (argsLength < 3)
    exitWithError('give password as an argument')
else if (argsLength < 5 && argsLength > 3) 
    exitWithError('give password, name and phone number as arguments')

const password = process.argv[2]

const url = 
    `mongodb+srv://fullstack:${password}@cluster0.bakbqg3.mongodb.net/perssonsApp?retryWrites=true&w=majority`

mongoose.set('strictQuery',false)
mongoose.connect(url)

const personSchema = new mongoose.Schema({
    name: String,
    phoneNumber: String
})

const Person = mongoose.model('Person', personSchema)

if (argsLength === 3) {
    console.log('phonebook:')
    Person.find({}).then(result => {
        result.forEach(({name, phoneNumber}) => console.log(`${name} ${phoneNumber}`))
        mongoose.connection.close()
    })
} else {
    const name = process.argv[3]
    const phoneNumber = process.argv[4]
    const person = new Person({name, phoneNumber})

    person.save().then(() => {
        console.log(`added ${name} number ${phoneNumber} to phonebook`)
        mongoose.connection.close()
    })
}