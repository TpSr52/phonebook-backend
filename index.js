require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

const app = express()
morgan.token('body', function (req, res) { return JSON.stringify(req.body) })

app.use(cors())
app.use(express.static('build'))
app.use(express.json())
app.use(morgan(':method :url :status :res[content-length] - :response-time ms :body'))

const Person = require('./models/person')

app.get('/info', (request, response) => {

    Person.countDocuments({}).then(num => {
        response.send(
            `<p>Phonebook has info for ${num} people</p>
            <p>${new Date()}</p>`
        )
    })
})

app.get('/api/persons', (request, response) => {
    Person.find({}).then(persons => response.json(persons))
})

app.post('/api/persons', (request, response, next) => {
    const { name, number } = request.body

    Person.find({ name: name })
        .then(result => {
            if (result) {
                response.status(409).end()
            }
        })
        .catch(error => next(error))

    if (!name || !number) {
        return response.status(400).json({ error: "Name or phone number is missing" })
    }


    const person = new Person({ name, number })

    person.save().then(result => {
        response.json(person)
    }).catch(error => next(error))

})

app.get('/api/persons/:id', (request, response) => {
    Person.findById(request.params.id).then(person => {
        response.json(person)
    }).catch(
        response.status(404).end()
    )
})

app.put('/api/persons/:id', (request, response, next) => {
    const { name, number } = request.body

    Person.findByIdAndUpdate(
        request.params.id,
        { name, number },
        { new: true, runValidators: true, context: 'query' })
        .then(returnedPerson => response.json(returnedPerson))
        .catch(error => next(error))
})

app.delete('/api/persons/:id', (request, response, next) => {
    Person.findByIdAndDelete(request.params.id)
        .then(result => response.status(204).end())
        .catch(error => next(error))
})

const errorHasndler = (error, request, response, next) => {
    console.error(error.message)

    if (error.name === 'CastError') {
        return response.status(400).send({ error: 'malformatted id' })
    } else if (error.name === 'ValidationError') {
        return response.status(400).json({ error: error.message })
    }

    next(error)
}

app.use(errorHasndler)

const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
